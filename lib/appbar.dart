import 'package:flutter/material.dart';
import 'package:midterm_reg/screen/login.dart';

enum SampleItem { itemOne, itemTwo }

AppBar MyAppbar(context) {
  SampleItem? selectedMenu;
  return AppBar(
    centerTitle: false,
    toolbarHeight: 70,
    elevation: 0,
    backgroundColor: Color.fromARGB(227, 239, 180, 32),
    leadingWidth: 90,
    leading: Row(
      children: const [
        SizedBox(
          width: 20,
        ),
        CircleAvatar(
          radius: 28,
          backgroundImage: NetworkImage(
              'https://upload.wikimedia.org/wikipedia/commons/e/ec/Buu-logo11.png'),
        ),
      ],
    ),
    title: FittedBox(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: const [
          Text(
            "ระบบบริการการศึกษา",
            style: TextStyle(
                fontWeight: FontWeight.w700, fontSize: 20, color: Colors.black),
          ),
          Text(
            'มหาวิทยาลัยบูรพา',
            style: TextStyle(fontSize: 14, color: Colors.black54),
          ),
        ],
      ),
    ),
    titleSpacing: 10,
    actions: [
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: InkWell(
          onTap: () {},
          mouseCursor: SystemMouseCursors.click,
          child: PopupMenuButton(
            child: CircleAvatar(
              radius: 20,
              backgroundImage: NetworkImage(
                  'https://media.thaigov.go.th/uploads/thumbnail/news/2020/04/IMG_28918_20200413104457000000.jpg'),
            ),
            initialValue: selectedMenu,
            itemBuilder: (BuildContext context2) =>
                <PopupMenuEntry<SampleItem>>[
              const PopupMenuItem<SampleItem>(
                // value: SampleItem.itemOne,
                child: Text('ข้อมูลส่วนตัว'),
              ),
              PopupMenuItem<SampleItem>(
                // value: SampleItem.itemTwo,
                child: GestureDetector(
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => const Login(),
                    ),
                  ),
                  child: Row(
                    children: [
                      Icon(Icons.logout_outlined, color: Colors.red),
                      SizedBox(
                        width: 2,
                      ),
                      Text(
                        'ออกจากระบบ',
                        style: TextStyle(color: Colors.red),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
          // CircleAvatar(
          //   radius: 20,
          //   backgroundImage: NetworkImage(
          //       'https://media.thaigov.go.th/uploads/thumbnail/news/2020/04/IMG_28918_20200413104457000000.jpg'),
          // ),
        ),
      ),
    ],
  );
}
