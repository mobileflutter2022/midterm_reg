import 'package:flutter/material.dart';
import 'package:marquee/marquee.dart';

class home extends StatelessWidget {
  const home(BuildContext context, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    var cardList = Container(
      height: screenWidth >= 1024 ? 400 : 260,
      child: ListView(
        physics: const AlwaysScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        children: [
          Card(
            margin: EdgeInsets.all(8),
            elevation: 2,
            child: SizedBox(
              width: screenWidth >= 1024 ? 520 : 360,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(30),
                child: Image(
                    image: AssetImage('assets/images/card1.png'),
                    fit: BoxFit.cover),
              ),
            ),
          ),
          Card(
            margin: EdgeInsets.all(8),
            elevation: 2,
            child: SizedBox(
              width: screenWidth >= 1024 ? 520 : 360,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(30),
                child: Image(
                    image: AssetImage('assets/images/card2.jpg'),
                    fit: BoxFit.cover),
              ),
            ),
          ),
          Card(
            margin: EdgeInsets.all(8),
            elevation: 2,
            child: SizedBox(
              width: screenWidth >= 1024 ? 520 : 360,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(30),
                child: Image(
                    image: AssetImage('assets/images/card3.jpg'),
                    fit: BoxFit.cover),
              ),
            ),
          ),
          Card(
            margin: EdgeInsets.all(8),
            elevation: 2,
            child: SizedBox(
              width: screenWidth >= 1024 ? 520 : 360,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(30),
                child: Image(
                    image: AssetImage('assets/images/card4.jpg'),
                    fit: BoxFit.cover),
              ),
            ),
          ),
          Card(
            margin: EdgeInsets.all(8),
            elevation: 2,
            child: SizedBox(
              width: screenWidth >= 1024 ? 520 : 360,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(30),
                child: Image(
                    image: AssetImage('assets/images/card1.png'),
                    fit: BoxFit.cover),
              ),
            ),
          ),
        ],
      ),
    );

    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            color: Colors.amber[100],
            height: 40,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Marquee(
                fadingEdgeStartFraction: 0.1,
                fadingEdgeEndFraction: 0.1,
                text: 'ยินดีต้อนรับสู่ระบบริการการศึกษามหาวิทยาลัยบูรพา',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                    color: Colors.deepOrange),
                scrollAxis: Axis.horizontal,
                crossAxisAlignment: CrossAxisAlignment.start,
                blankSpace: 250.0,
                velocity: 50.0,
                startPadding: 10.0,
              ),
            ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: const Padding(
              padding: EdgeInsets.all(13.0),
              child: Text(
                'ประกาศ',
                style: TextStyle(fontSize: 18),
              ),
            ),
          ),
          cardList,
          Align(
            alignment: Alignment.topLeft,
            child: const Padding(
              padding: EdgeInsets.all(13.0),
              child: Text(
                'เครื่องมือนิสิต',
                style: TextStyle(fontSize: 18),
              ),
            ),
          ),
          Container(
            // height: 300,
            width: MediaQuery.of(context).size.width * 0.89,
            child: Card(
              elevation: 1,
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Wrap(
                  spacing: 15,
                  alignment: WrapAlignment.start,
                  runAlignment: WrapAlignment.start,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  runSpacing: 15,
                  direction: Axis.horizontal,
                  children: [
                    FloatingActionButton.extended(
                      heroTag: null,
                      isExtended: screenWidth >= 600 ? true : false,
                      elevation: 2,
                      backgroundColor: Color.fromARGB(227, 239, 180, 32),
                      onPressed: () {},
                      label: Text('ลงทะเบียน'),
                      icon: Icon(Icons.add_circle_outline),
                    ),
                    FloatingActionButton.extended(
                      heroTag: null,
                      isExtended: screenWidth >= 600 ? true : false,
                      elevation: 2,
                      backgroundColor: Color.fromARGB(227, 239, 180, 32),
                      onPressed: () {},
                      label: Text('ผลการลงทะเบียน'),
                      icon: Icon(Icons.list_alt_rounded),
                    ),
                    FloatingActionButton.extended(
                      heroTag: null,
                      isExtended: screenWidth >= 600 ? true : false,
                      elevation: 2,
                      backgroundColor: Color.fromARGB(227, 239, 180, 32),
                      onPressed: () {},
                      label: Text('ผลอนุมัติเพิ่ม-ลด'),
                      icon: Icon(Icons.list_alt_rounded),
                    ),
                    FloatingActionButton.extended(
                      heroTag: null,
                      isExtended: screenWidth >= 600 ? true : false,
                      elevation: 2,
                      backgroundColor: Color.fromARGB(227, 239, 180, 32),
                      onPressed: () {},
                      label: Text('ภาระค่าใช้จ่ายทุน'),
                      icon: Icon(Icons.payment_rounded),
                    ),
                    FloatingActionButton.extended(
                      heroTag: null,
                      isExtended: screenWidth >= 600 ? true : false,
                      elevation: 2,
                      backgroundColor: Color.fromARGB(227, 239, 180, 32),
                      onPressed: () {},
                      label: Text('ตรวจสอบจบ'),
                      icon: Icon(Icons.checklist_rtl_rounded),
                    ),
                    FloatingActionButton.extended(
                      heroTag: null,
                      isExtended: screenWidth >= 600 ? true : false,
                      elevation: 2,
                      backgroundColor: Color.fromARGB(227, 239, 180, 32),
                      onPressed: () {},
                      label: Text('ยื่นคำร้อง'),
                      icon: Icon(Icons.paste_rounded),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
