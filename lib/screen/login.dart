import 'package:flutter/material.dart';
import '../homepage.dart';

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
      child: MaterialApp(
        theme: ThemeData(
          fontFamily: 'BalsamiqSans',
          primarySwatch: Colors.amber,
          useMaterial3: true,
        ),
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          body: SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  Image(
                    image: AssetImage('assets/images/login-logo.jpg'),
                    fit: BoxFit.cover,
                    height: size.height *0.2,
                  ),
                  SizedBox(
                    height: 6,
                  ),
                  Text(
                    'เข้าสู่ระบบ',
                    style: Theme.of(context).textTheme.headline3,
                  ),
                  Form(
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextFormField(
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.person_outlined),
                                labelText: 'ชื่อผู้ใช้',
                                hintText: 'ชื่อผู้ใช้',
                                border: OutlineInputBorder()),
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          TextFormField(
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.person_outline),
                                labelText: 'รหัสผ่าน',
                                hintText: 'รหัสผ่าน',
                                border: OutlineInputBorder(),
                                suffixIcon: IconButton(
                                  onPressed: null,
                                  icon: Icon(Icons.remove_red_eye_sharp),
                                )),
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: TextButton(
                              onPressed: () {},
                              child: Text('ลืมรหัสผ่าน'),
                            ),
                          ),
                          SizedBox(
                            width: double.infinity,
                            child: ElevatedButton(
                              onPressed: () => Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => const homepage(),
                                ),
                              ),
                              child: Text(
                                'เข้าสู่ระบบ',
                                style: TextStyle(fontSize: 30),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
