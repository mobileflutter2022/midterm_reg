import 'package:flutter/material.dart';

class grade extends StatelessWidget {
  const grade(BuildContext context, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: screenWidth >= 1024
            ? CrossAxisAlignment.start
            : CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Card(
              elevation: 4,
              child: Container(
                width:
                    screenWidth >= 1024 ? screenWidth * 0.2 : screenWidth * 0.5,
                child: Image(
                  fit: BoxFit.cover,
                  image: NetworkImage(
                      'https://media.thaigov.go.th/uploads/thumbnail/news/2020/04/IMG_28918_20200413104457000000.jpg'),
                ),
              ),
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.02,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Container(
              width:
                  screenWidth >= 1024 ? screenWidth * 0.4 : screenWidth * 0.85,
              child: Image(
                image: AssetImage('assets/images/grade1.png'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Container(
              width:
                  screenWidth >= 1024 ? screenWidth * 0.4 : screenWidth * 0.85,
              child: Image(
                image: AssetImage('assets/images/grade2.png'),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
