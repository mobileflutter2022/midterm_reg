import 'package:flutter/material.dart';
import 'package:midterm_reg/screen/grade.dart';
import 'package:midterm_reg/screen/home.dart';
import 'package:midterm_reg/screen/schedule.dart';

import 'appbar.dart';

class homepage extends StatefulWidget {
  const homepage({Key? key}) : super(key: key);

  @override
  _homepageState createState() => _homepageState();
}

class _homepageState extends State<homepage> {
  // final List<Widget> _screens = [
  //   home(),
  //   schedule(),
  //   grade(),
  // ];

  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final List<Widget> _screens = [
      home(context),
      schedule(context),
      grade(context),
    ];
    return SafeArea(
      child: Scaffold(
        appBar: MyAppbar(context),
        bottomNavigationBar: screenWidth >= 1024
            ? null
            : Container(height: 60, child: bottomNavigationBar(context)),
        body: SingleChildScrollView(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              screenWidth >= 1024
                  ? navigation_rail(context)
                  : SizedBox(
                      height: 0,
                      width: 0,
                    ),
              Expanded(child: _screens[_selectedIndex])
            ],
          ),
        ),
      ),
    );
  }

  BottomNavigationBar bottomNavigationBar(BuildContext context) {
    return BottomNavigationBar(
      backgroundColor: Colors.grey[100],
      elevation: 8,
      currentIndex: _selectedIndex,
      unselectedItemColor: Colors.grey,
      selectedItemColor: Color.fromARGB(227, 239, 180, 32),
      onTap: (int index) {
        setState(() {
          _selectedIndex = index;
        });
      },
      items: const [
        BottomNavigationBarItem(
            activeIcon: Icon(Icons.home),
            icon: Icon(Icons.home_outlined),
            label: 'หน้าหลัก'),
        BottomNavigationBarItem(
            activeIcon: Icon(Icons.list),
            icon: Icon(Icons.list_alt_rounded),
            label: 'ตาราเรียน/สอบ'),
        BottomNavigationBarItem(
            activeIcon: Icon(Icons.school),
            icon: Icon(Icons.school_outlined),
            label: 'ผลการศึกษา')
      ],
    );
  }

  Container navigation_rail(BuildContext context) {
    return Container(
      width: 200,
      height: MediaQuery.of(context).size.height - 70,
      decoration: BoxDecoration(color: Colors.white),
      child: NavigationRail(
        extended: true,
        backgroundColor: Colors.white,
        groupAlignment: -1.0,
        onDestinationSelected: (int index) {
          setState(() {
            _selectedIndex = index;
          });
        },
        selectedIndex: _selectedIndex,
        destinations: const <NavigationRailDestination>[
          NavigationRailDestination(
            icon: Icon(Icons.house_outlined),
            selectedIcon: Icon(Icons.house),
            label: Text(
              'หน้าหลัก',
            ),
          ),
          NavigationRailDestination(
            icon: Icon(Icons.circle_outlined, size: 15),
            selectedIcon: Icon(Icons.circle, size: 15),
            label: Text('ตารางเรียน/สอบ'),
          ),
          NavigationRailDestination(
            icon: Icon(Icons.circle_outlined, size: 15),
            selectedIcon: Icon(Icons.circle, size: 15),
            label: Text('ผลการศึกษา'),
          ),
        ],
        labelType: NavigationRailLabelType.none,
      ),
    );
  }
}
